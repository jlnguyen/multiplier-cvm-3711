--==============================================================
/*
SCRIPT:
    a02_ds_load.sql

PURPOSE:
    - Load allocated offers into ca002_

INPUTS:

OUTPUTS:
    loyalty_modeling.cvm3711_ds_load
    ca002_model_audience_list_inbound

DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 17Aug2020
    ---
*/
--==============================================================
-------------------------------------------------


drop table if exists loyalty_modeling.cvm3711_ds_load;
create table loyalty_modeling.cvm3711_ds_load
(
    ref_dt              date
    ,crn                varchar(20)
    ,audience_type      varchar(5)
    ,offer_id           varchar(20)
    ,offer_type         varchar(10)
    ,camp_dur_wks       smallint
    ,spend_hurdle       numeric(10,2)
    ,reward             numeric(10,2)
    ,multiplier         smallint
    ,shopped_last_12wk  smallint
    ,open_email_26wk    smallint
    ,is_engaged         smallint
    ,inc_sales          numeric(18,6)
    ,p_rdm              numeric(18,6)
    ,spd_rdm            numeric(10,2)
    ,spd_not_rdm        numeric(10,2)
    ,base_spd           numeric(10,2)
    ,target_spd         numeric(10,2)
    ,decile             smallint
    ,macro_segment_curr varchar(20)
    ,cvm_segment        varchar(20)
    ,insert_date        date
)
distkey (crn)
;
-- grant select on loyalty_modeling.mel_ds_load_20200629 to public;

copy loyalty_modeling.cvm3711_ds_load
from 's3://data-preprod-redshift-exports/Joe/a01_melon/cvm3711_20200826_send.csv'
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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' 
CSV IGNOREHEADER 1;
-- select * from stl_load_errors;
--^ Warning [01000 / 0]: Load into table 'mel_ds_load_20200720' completed, 10063323 record(s) loaded successfully.
select count(*), count(distinct crn) from loyalty_modeling.cvm3711_ds_load; -- [10063323	10063323]


-------------------------------------------------
-- Insert into campaign table
-------------------------------------------------
insert into loyalty_bi_analytics.ca002_model_audience_list_inbound
select
    bse.campaign_code
    ,bse.campaign_start_date
    ,bse.crn
    ,'MP' as var01
    ,bse.camp_dur_wks as var02
    ,'store' as var03
    ,bse.spend_hurdle as var04
    ,0 as var05     -- reward
    ,NULL as var06  -- spend band
    ,NULL as var07  -- C/D
    ,bse.offer_id as var08
    ,ds.audience_type as var09
    ,'' as var10
    ,'JN' as insert_by
    ,ds.insert_date as score_date
-- select count(*)  -- [10,152,513]
from loyalty_modeling.cvm3711_score_base as bse
join
    loyalty_modeling.cvm3711_ds_load as ds
on ds.crn = bse.crn;
--^ [10,152,513 rows affected]

-- select top 2 * from loyalty_bi_analytics.ca002_model_audience_list_inbound
-- where campaign_start_date = '2020-07-20' and var01 = 'MP'

/* View sample data */
select top 188
    cn.*
    ,bse.*
from loyalty_modeling.cvm3711_ds_load as bse
right join
    loyalty_modeling.jn_cust_name as cn
on cn.crn = bse.crn;
