--==============================================================
/*
SCRIPT:
    a01_score_base_prep.sql

PURPOSE:
    - Prepare audience list for scoring
    - Identify engaged customers because MP model was only trained
    on engaged customers

INPUTS:
    loyalty_bi_analytics.ca001_model_audience_list_outbound
    loyalty_campaign_analytics.ca_master_customer_profiling
    
OUTPUTS:

DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 14Aug2020
    ---
*/
--==============================================================
-------------------------------------------------


drop table if exists loyalty_modeling.cvm3711_score_base;
create table loyalty_modeling.cvm3711_score_base
distkey (crn)
as
with reference as (
    select
        '2020-08-09'::date as ref_dt
        ,'2020-08-26'::date as send_dt
        ,'CVM-3711'::varchar(10) as camp_code
)
,base as
(
    select
        rf.ref_dt
        ,bse.crn
        ,bse.campaign_code
        ,bse.campaign_start_date
        -- ,bse.var03 as offer_type
        ,bse.var08 as offer_id
        ,'4. MP'::varchar(10) as campaign_type
        ,bse.var06 as camp_dur_wks
        ,isnull(bse.var04::smallint, '0')::int as multiplier
        ,isnull(bse.var01, '0')::int as spend_hurdle
        ,cast(
            isnull(cvm."8wk_spend", 0) * (multiplier - 1) / 200
            as numeric(10,2)
        ) as reward
        -- ,isnull(var02, '0') as reward
        ,0 as camp_rdm_flag
        ,isnull(cvm."8wk_spend", 0) as spend_8wk
    from loyalty_bi_analytics.ca001_model_audience_list_outbound as bse
    cross join reference as rf
    left join
        loyalty.customer_value_model as cvm
    on bse.crn = cvm.crn
        and rf.ref_dt - 5 = cvm.pw_end_date
    where campaign_start_date = rf.send_dt
        and campaign_code = rf.camp_code
)
-- select * from base limit 10
select
    base.*
    ,max(case when mcp.cvm_weeks_since_last_shop <= 12 then 1 else 0 end) as shopped_last_12wk
    ,max(case when mcp.open_an_email_or_activate_offer_26wk = 'Y' then 1 else 0 end) as open_email_26wk
    ,max(case when mcp.cvm_weeks_since_last_shop <= 12
            and mcp.open_an_email_or_activate_offer_26wk = 'Y'
        then 1 else 0
    end) as is_engaged
from base
left join
    loyalty_campaign_analytics.ca_master_customer_profiling as mcp
on mcp.crn = base.crn
group by 1,2,3,4,5,6,7,8,9,10,11,12
;
-- select count(*) from loyalty_modeling.cvm3711_score_base;   -- [10,152,513]
-- select crn, count(*) from loyalty_modeling.cvm3711_score_base group by 1 having count(*) > 1 limit 10;  -- [EMPTY]
-- select count(*) from loyalty_bi_analytics.ca001_model_audience_list_outbound where campaign_code = 'CVM-3711';   -- [10,152,513]

/* View samples */
select cn.*, bse.*
from loyalty_modeling.cvm3711_score_base as bse
right join loyalty_modeling.jn_cust_name as cn
on cn.crn = bse.crn
order by 1;

select is_engaged, count(*)
from loyalty_modeling.cvm3711_score_base
group by 1 order by 1;

-------------------------------------------------
-- Upload to S3
-------------------------------------------------
UNLOAD ('select * from loyalty_modeling.cvm3711_score_base')
TO 's3://data-preprod-redshift-exports/Joe/a01_melon/cvm3711_score_base_20200826'
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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'
FORMAT PARQUET
ALLOWOVERWRITE
PARALLEL OFF;